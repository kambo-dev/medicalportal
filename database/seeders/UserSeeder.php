<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Ramsey\Uuid\Type\Integer;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => Str::random(6),
            'last_name' => Str::random(6),
            'email' => Str::random(10).'@gmail.com',
//            'email' => 'admin@admin.com',
            'role_id' => 2,
            'password' => Hash::make('password'),
            'field' => 'dentist'
        ]);
    }
}
