<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('appointments')->insert([
            'date' => Carbon::createFromDate(2021,01,01)->toDateString(),
            'time' => Carbon::createFromDate(2021,01,01)->toDateTimeString(),
            'description' => Str::random(10),
            'patient_id' => 4,
            'doctor_id' => 14,
        ]);
    }
}
