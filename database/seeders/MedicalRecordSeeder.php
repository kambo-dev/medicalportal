<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class MedicalRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medical_records')->insert([
            'illness' => Str::random(6),
            'data' => Str::random(10),
            'note' => Str::random(10),
            'user_id' => 5,
        ]);
    }
}
