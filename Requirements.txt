Ndertimi i nje portali per mjeket dhe pacientet.

1. Nje modul ku pacienti mund te rregjistrohet/logohet.

	A. Pacintet mund te rezervojne takime ne portal bazuar 
	ne kohen e disponueshme te mjekeve.

	B. Portali do tju tregoje oraret e lira te mjekeve
	dhe pacienti mund te zgjedhe njerin prej tyre.

	C. Bazuar ne specializimet/eksperiencen e mjekeve, pacientit mund ti
	rekomandohen mjeke te caktuar.

pacients {
	[1]{
		first_name = '',
		last_name = '',
		email = '',
		id = '',
		medical_record = {
			[1]{
				title = '',
				data = '',
				note = ''
			},
			[2]{
				title = '',
				data = '',
				note = ''
			}
		}
	},
	[2]{},
}

2. Nje modul ku mjeku mund te rregjistrohet/logohet.

	A. Mjeket do te munden te menaxhojne takimet e tyre.

	B. Mjeket do te munden te ruajne te dhenat mjekesore te
	pacienteve si dhe te lexojne ato ekzistuese.


doctors {
	[1]{
		first_name = '',
		last_name = '',
		email = '',
		id = '',
		work_field = '',
		rating = '',
		appoiments = {
			[1]{
				start_ = '',
				end_ = ''
			},
			[2]{
				start_ = '',
				end_ = ''
			}
		}
	},
	[2]{},
	[3]{},
}

