@extends('layouts.app')

@section('content')
@if(Auth::user()->hasRole('patient'))

    <p class="login-box-msg">Book an appointment</p>
    <form action="{{ route('book.store') }}" class="border-bottom pb-4" method="post">
        @csrf
        @method('POST')
        <!-- Type of appointment -->
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Looking for a</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="input-group">
                    <select class="form-control col-8 border-right" name="field" id="field">
                        @foreach($fields as $field)
                            <option value="{{$field['field']}}">
                                {{$field['field']}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <!-- Time of appointment -->
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Select date & time</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="input-group">
                    <input class="form-control string required" type="text" name="time" id="time">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <span class="fa fa-calendar"></span>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <!-- Doctor selector -->
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Select your doctor</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="input-group">
                    <select class="form-control col-8 border-right" name="doctor_id" id="doctor">
                    </select>
                </div>
            </div>
        </div>
        <!-- Note -->
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Add a note</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="input-group">
                    <textarea class="form-control border-right" id="description" name="description"
                              rows="2" placeholder="Optional"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-3"></div>
            <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block mx-auto">Book</button>
            </div>
        </div>
    </form>
@endif
    <!-- Active appointments -->
<p class="login-box-msg m-3">Active appointments</p>
    <div class="row">

        @if( $appointments->isEmpty() )
            <h5 class="login-box-msg mt-4" style="width: 100%">You don't have any active appointments.</h5>
        @else
            @foreach($appointments as $appointment)
                <div class="card my-4 mx-2 py-4">
                    <h4 class="login-box-msg">Appointment [#{{$appointment->id}}]</h4>
                    @if(Auth::user()->hasRole(\App\Enums\RoleEnum::PATIENT))
                        <h5 class="login-box-msg">Doctor's name: </h5>
                        <p class="login-box-msg">
                            {{$appointment->doctor->first_name}}
                            {{$appointment->doctor->last_name}}
                        </p>
                    @elseif (Auth::user()->hasRole(\App\Enums\RoleEnum::DOCTOR))
                        <h5 class="login-box-msg">Patient's name: </h5>
                        <p class="login-box-msg">
                            {{$appointment->patient->first_name}}
                            {{$appointment->patient->last_name}}
                        </p>
                    @endif
                    <h5 class="login-box-msg">Time of appointment: </h5>
                    <p class="login-box-msg">
                        {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $appointment->time)
                        ->format('d/m/Y H:i')}}
                    </p>
                    @if(Auth::user()->hasRole(\App\Enums\RoleEnum::DOCTOR))
                        <div class="row my-2">
                            <div class="col text-center">
                                <a href="{{ route('medrecord', $appointment->patient->id) }}" class="btn btn-sm btn-outline-success">
                                    Medical records
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-center">
                                <form action="{{ route('book.delete', $appointment->id) }}" method="post" class="form-group">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-outline-danger mx-5">
                                        Close appointment
                                    </button>
                                </form>
                            </div>
                        </div>
                    @endif
                    @if(Auth::user()->hasRole(\App\Enums\RoleEnum::PATIENT))
                        <div class="row">
                            <div class="col text-center">
                                <form action="{{ route('rate', ['doctor_id' => $appointment->doctor_id, 'appointment_id' => $appointment->id]) }}" method="post" class="form-group">
                                    @csrf
                                    @method('POST')
                                    <select class="rating" name="rating">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <button type="submit" class="btn btn-sm btn-outline-primary mx-5">
                                        Rate doctor
                                    </button>
                                </form>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
        @endif
    </div>

@endsection
@push('scripts')
    <script src="{{asset('js/jquery.barrating.min.js')}}"></script>

    <script type="text/javascript">

        $(function() {
            jQuery('.rating').each(function() {
                $('.rating').barrating({
                    theme: 'fontawesome-stars'
                });
            })
        });

        $(function() {
            $('input[name="time"]').daterangepicker({
                timePicker: true,
                timePicker24Hour: true,
                timePickerIncrement: 60,
                singleDatePicker: true,
                startDate: moment().startOf('hour'),
                locale: {
                    format: 'YYYY-MM-DD HH:00'
                }
            });
        });

        $('#time').on('hide.daterangepicker', function(ev, picker) {
            getDoctorsList();
        });

        function getDoctorsList()
        {
            var field = $('#field').val();
            var time = $('#time').val();

            $.ajax({
                url: 'book/getData/' + field + '/' + time,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    $('#doctor').children().remove().end();
                    $.each(response.data, function (key, value) {
                        $('#doctor').append('' +
                            '<option value="'+value.id+'">'+value.first_name+' '+value.last_name+'</option>'
                        );
                    })
                }
            });
        }
    </script>
@endpush
