@extends('layouts.app')

@section('content')
    @if(Auth::user()->hasRole(\App\Enums\RoleEnum::DOCTOR))
    <p class="login-box-msg text-center">Update medical records of {{$user->first_name}} {{$user->last_name}}</p>
    <form action="{{ route('medrecord.store', $user->id) }}" class="border-bottom pb-4" method="post">
    @csrf
        @method('POST')
    <!-- Illness -->
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Illness name</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="input-group">
                    <input class="form-control border-right required" type="text" name="illness" id="illness">
                </div>
            </div>
        </div>
        <!-- Data -->
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Data about this illness</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="input-group">
                    <textarea class="form-control border-right required" type="text" name="data" id="data">
                    </textarea>
                </div>
            </div>
        </div>
        <!-- Note -->
        <div class="form-group row">
            <label class="col-form-label text-right col-lg-3 col-sm-12">Add a note</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                <div class="input-group">
                    <textarea class="form-control border-right required" type="text" name="note" id="note">
                    </textarea>
                </div>
            </div>
        </div>

        <div id="medicamentForm">
            <div id="medicamentForm-0">
            <!-- Medicament name -->
                <div class="form-group row">
                    <label class="col-form-label text-right col-lg-3 col-sm-12">Medicament name</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="input-group">
                            <input class="form-control border-right" id="med_name" name="medicaments[0][name]">
                        </div>
                    </div>
                    <span class="btn btn-sm btn-danger" data-med_id="0" onclick="removeMedicament(this.dataset.med_id)">
                        <i class="fa fa-times px-2"></i>
                    </span>
                </div>
                <!-- Medicament description -->
                <div class="form-group row">
                    <label class="col-form-label text-right col-lg-3 col-sm-12">Medicament prescription</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <div class="input-group">
                            <textarea class="form-control border-right" id="med_prescription" name="medicaments[0][description]">
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row medicamentAdd">
            <div class="col-3"></div>
            <div class="col-4">
                <div class="btn btn-secondary btn-block my-2" onclick="addMedicament()">Add medicament</div>
            </div>
        </div>

        <div class="row">
            <div class="col-3"></div>
            <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block mx-auto">Update medical records</button>
            </div>
        </div>
    </form>
    @endif

    <p class="login-box-msg m-3">Medical records of {{$user->first_name}} {{$user->last_name}}</p>
    @foreach($user['medicalRecords'] as $medicalRecord)
    <div class="jumbotron">
        <h4 class="text-center">
            Record: #{{$medicalRecord->id}} |
            {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$medicalRecord->updated_at)
            ->format('d/m/Y')}}
        </h4>
        <div class="row">
            <div class="col"><p>Illness: {{$medicalRecord->illness}}</p></div>
            <div class="col"><p>Illness data: {{$medicalRecord->data}}</p></div>
            <div class="col"><p>Illness note: {{$medicalRecord->note}}</p></div>
        </div>
        @foreach($user['medicaments'] as $medicament)
            @if($medicament->medical_record_id == $medicalRecord->id)
            <div class="row">
                <div class="col"><p>Medicament name: {{$medicament->name}}</p></div>
                <div class="col"><p>Prescription: {{$medicament->description}}</p></div>
            </div>
            @endif
        @endforeach
        @if(Auth::user()->hasRole(\App\Enums\RoleEnum::DOCTOR))
        <form action="{{ route('medrecord.delete', $medicalRecord->id) }}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-sm btn-outline-danger">Delete record</button>
        </form>
        @endif
    </div>
    @endforeach
@endsection

@push('scripts')
    <script type="text/javascript">
        function addMedicament()
        {
            if ( typeof this.counter == 'undefined' ) {
                this.counter = 1;
            }

            $('#medicamentForm').append(
                "<div id='medicamentForm-"+this.counter+"'>" +
                    "<div class='form-group row' >" +
                    "<label class='col-form-label text-right col-lg-3 col-sm-12'>Medicament name</label>" +
                        "<div class='col-lg-4 col-md-9 col-sm-12'>" +
                            "<div class='input-group'>" +
                                "<input class='form-control border-right' id='med_name' name='medicaments["+this.counter+"][name]'>" +
                            "</div>" +
                        "</div>" +
                        "<span class='btn btn-sm btn-danger' data-med_id='"+this.counter+"' onclick='removeMedicament(this.dataset.med_id)'>" +
                            "<i class='fa fa-times px-2'></i>" +
                        "</span>" +
                    "</div>" +
                    "<div class='form-group row'>" +
                        "<label class='col-form-label text-right col-lg-3 col-sm-12'>Medicament prescription</label>" +
                        "<div class='col-lg-4 col-md-9 col-sm-12'>" +
                            "<div class='input-group'>" +
                                "<textarea class='form-control border-right' id='med_prescription' name='medicaments["+this.counter+"][description]'>" +
                                "</textarea>" +
                            "</div>" +
                        "</div>" +
                    "</div>" +
                "</div>"
            );
            this.counter++;
        }

        function removeMedicament(id)
        {
            $('#medicamentForm-' + id).remove();
        }
    </script>
@endpush
