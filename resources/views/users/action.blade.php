@if ( $deleted_at == null )
    @if(Auth::id() !== $id)
        <form action="{{ route('users.destroy', ['user' => $id]) }}" method="post" class="form">
            @csrf
            @method('DELETE')
            <div class="mb-1">
                <button type="submit" class="btn btn-sm btn-warning">
                    Block
                </button>
            </div>
        </form>
<!-- Button trigger Edit User -->
<button type="button" class="btn btn-sm btn-primary mb-1" onclick="editUser({{$id}})">
    Edit
</button>
    @endif
@else
<form action="{{ route('users.restore', ['user' => $id]) }}" method="post" class="form">
    @csrf
    @method('PUT')
    <div class="mb-1">
        <button type="submit" class="btn btn-sm btn-success">
            Restore
        </button>
    </div>
</form>
@endif
@if(Auth::id() !== $id)
    <form action="{{ route('users.remove', ['user' => $id]) }}" method="post" class="form">
        @csrf
        @method('DELETE')
        <div class="form-group mb-0">
            <button type="submit" class="btn btn-sm btn-danger">
                Delete
            </button>
        </div>
    </form>
@endif
