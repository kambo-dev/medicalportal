@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <h1 class="lead">Table of users.</h1>

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addNewUser">
        Add new user
    </button>
<!-- Register new user Form -->
    <div class="modal fade" id="addNewUser" tabindex="-1" role="dialog" aria-labelledby="addNewUserlLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Register a new User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('users.store') }}" method="post">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First name">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last name">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Retype password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="col-4 pl-0">
                                <p class="text-center py-1">Select role:</p>
                            </div>
                            <select class="form-control col-8" name="role_id" id="role_id">
                                @foreach($roles as $id => $name)
                                    <option value="{{$id}}">{{$name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block">Register</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.form-box -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
<!-- Edit user Form -->
    <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="editUserLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit a User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" method="post" class="form" id="editUserForm">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <span class="error alert alert-danger container-fluid" style="display: none"></span>
                                <input type="text" name="first_name" id="first_name_edit" class="form-control" placeholder="First name">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-user"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <span class="error alert alert-danger container-fluid" style="display: none"></span>
                                <input type="text" name="last_name" id="last_name_edit" class="form-control" placeholder="Last name">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-user"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <span class="error alert alert-danger container-fluid" style="display: none"></span>
                                <input type="email" name="email" id="email_edit" class="form-control" placeholder="Email">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-envelope"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <span class="error alert alert-danger container-fluid" style="display: none"></span>
                                <input type="password" name="password" id="password_edit" class="form-control" placeholder="Password">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <input type="password" name="password_confirmation" id="password_confirmation_edit" class="form-control" placeholder="Password">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="col-4 pl-0">
                                    <p class="text-center py-1">Select role:</p>
                                </div>
                                <select class="form-control col-8" name="role_id" id="role_id_edit">
                                    @foreach($roles as $id => $name)
                                        <option value="{{$id}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="button" id="btn-save" data-user_id="#" class="btn btn-primary" value="Save" onclick="updateUser(this.dataset.user_id)">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{$dataTable->table()}}

@endsection

@push('scripts')
    {{$dataTable->scripts()}}

    <script type="text/javascript">
        function editUser(id){
            $.ajax({
                url: 'getData/'+ id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    //Reset validation errors
                    $('#editUserForm input[name="first_name"]').parent().find('.error').hide();
                    $('#editUserForm input[name="last_name"]').parent().find('.error').hide();
                    $('#editUserForm input[name="email"]').parent().find('.error').hide();
                    $('#editUserForm input[name="password"]').parent().find('.error').hide();

                    var selector = $(jQuery('#editUser'));
                    selector.find('input[type="button"]').attr('data-user_id', response['data']['id']);
                    selector.find('input[name="first_name"]').val(response['data']['first_name']);
                    selector.find('input[name="last_name"]').val(response['data']['last_name']);
                    selector.find('input[name="email"]').val(response['data']['email']);
                    selector.find('select[id="role_id_edit"]').val(response['data']['role_id']);
                }});

            $('#editUser').modal('show');
        }
        function updateUser(id){
            $.ajax({
                url: 'users/' + id,
                method: 'PUT',
                data: $('#editUserForm').serializeArray(),
                success:function(){
                    alert("Success!");
                },
                error: function (xhr) {
                    //Reset validation errors
                    $('#editUserForm input[name="first_name"]').parent().find('.error').hide();
                    $('#editUserForm input[name="last_name"]').parent().find('.error').hide();
                    $('#editUserForm input[name="email"]').parent().find('.error').hide();
                    $('#editUserForm input[name="password"]').parent().find('.error').hide();

                    //Show validation errors
                    $.each(xhr.responseJSON.errors, function(key,value) {
                        $('#editUserForm input[name="'+key+'"]').parent().find('.error').text(value).show();
                    });
                },
            });
        }
    </script>
@endpush
