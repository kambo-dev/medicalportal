<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MedicalRecordController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::middleware(['role:admin'])->group(function () {

    Route::resource('users', UserController::class);

    Route::put('users/{user}/restore',
        [UserController::class, 'restore']
    )->name('users.restore');

    Route::delete('users/{user}/delete',
        [UserController::class, 'remove']
    )->name('users.remove');

    Route::get('/getData/{id}',
        [UserController::class, 'getData']);

});

Route::resource('roles', RoleController::class);

Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/register',[RegisterController::class,'store'])->name('register.store');

Route::get('/login',[LoginController::class,'index'])->name('login');
Route::post('/login',[LoginController::class,'store']);
Route::get('/logout',[LoginController::class, 'logout'])->name('logout');

Route::middleware(['role:patient|doctor'])->group(function () {

    Route::get('/book',
        [BookingController::class, 'index'])
        ->name('book');

    Route::post('/book',
        [BookingController::class, 'store'])
        ->name('book.store');

    Route::get('/book/getData/{field}/{time}',
        [BookingController::class, 'getData']);

    Route::delete('/book/{id}',
        [BookingController::class, 'delete'])
        ->name('book.delete');
});

Route::middleware(['role:doctor|patient'])->group(function () {

    Route::get('/medical/records/{patient_id}',
        [MedicalRecordController::class, 'index'])
        ->name('medrecord');

    Route::post('medical/records/{patient_id}',
        [MedicalRecordController::class, 'store'])
        ->name('medrecord.store');
});

Route::middleware(['role:doctor'])->group(function () {

    Route::delete('medical/records/delete/{record_id}',
        [MedicalRecordController::class, 'delete'])
        ->name('medrecord.delete');
});

Route::middleware(['role:patient'])->group(function () {

    Route::post('/rate/{doctor_id}/{appointment_id}',
        [RatingController::class, 'store'])
        ->name('rate');
});

//Route::get('/calendar', [HomeController::class, 'calendar'])->name('calendar');
