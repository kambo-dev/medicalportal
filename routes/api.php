<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\MedicalRecordController;
use App\Http\Controllers\Api\V1\RatingController;
use App\Http\Controllers\Api\V1\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {

    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);

});

Route::middleware(['auth:api','role:doctor|patient'])->group(function () {

    Route::get('/v1/user/{patient_id}/medical/record/',
        [MedicalRecordController::class, 'index']
    );

});

Route::middleware(['auth:api','role:admin'])->prefix('admin')
    ->group(function () {

    Route::apiResource('/v1/',
        UserController::class
    );

    Route::put('/v1/{user}/restore',
        [UserController::class, 'restore']
    );

    Route::delete('/v1/{user}/delete',
        [UserController::class, 'remove']
    );

});

Route::middleware(['auth:api','role:patient'])->group(function () {

    Route::post(
        'v1/doctor/{doctor_id}/appointment/{appointment_id}',
        [RatingController::class, 'store']
    );

});

