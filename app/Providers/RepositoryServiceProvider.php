<?php

namespace App\Providers;

use App\Repositories\Contracts\AppointmentRepositoryContract;
use App\Repositories\Contracts\BaseRepositoryContract;
use App\Repositories\Contracts\MedicalRecordRepositoryContract;
use App\Repositories\Contracts\MedicamentRepositoryContract;
use App\Repositories\Contracts\RatingRepositoryContract;
use App\Repositories\Eloquent\AppointmentRepository;
use App\Repositories\Eloquent\BaseRepository;
use App\Repositories\Contracts\UserRepositoryContract;
use App\Repositories\Eloquent\MedicalRecordRepository;
use App\Repositories\Eloquent\MedicamentRepository;
use App\Repositories\Eloquent\RatingRepository;
use App\Repositories\Eloquent\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseRepositoryContract::class, BaseRepository::class);
        $this->app->bind(UserRepositoryContract::class, UserRepository::class);
        $this->app->bind(AppointmentRepositoryContract::class, AppointmentRepository::class);
        $this->app->bind(MedicalRecordRepositoryContract::class, MedicalRecordRepository::class);
        $this->app->bind(MedicamentRepositoryContract::class, MedicamentRepository::class);
        $this->app->bind(RatingRepositoryContract::class, RatingRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
