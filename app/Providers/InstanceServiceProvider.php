<?php


namespace App\Providers;


use App\Http\Services\AppointmentService;
use App\Http\Services\MedicalRecordService;
use App\Http\Services\RatingService;
use App\Http\Services\UserService;
use App\Repositories\Eloquent\AppointmentRepository;
use App\Repositories\Eloquent\MedicalRecordRepository;
use App\Repositories\Eloquent\MedicamentRepository;
use App\Repositories\Eloquent\RatingRepository;
use App\Repositories\Eloquent\UserRepository;
use Illuminate\Support\ServiceProvider;

class InstanceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    public function boot()
    {
        $this->app->instance(UserService::class,
            new UserService(
                new UserRepository($this->app)
            )
        );

        $this->app->instance(AppointmentService::class,
            new AppointmentService(
                new AppointmentRepository($this->app)
            )
        );

        $this->app->instance(MedicalRecordService::class,
            new MedicalRecordService(
                new MedicalRecordRepository($this->app),
                new MedicamentRepository($this->app)
            )
        );

        $this->app->instance(RatingService::class,
            new RatingService(
                new RatingRepository($this->app)
            )
        );
    }
}
