<?php

namespace App\Enums;

final class RoleEnum extends Enum
{
    public const ADMIN = 'admin';
    public const DOCTOR = 'doctor';
    public const PATIENT = 'patient';
}
