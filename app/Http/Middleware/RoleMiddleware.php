<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param $role
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $roles)
    {
        $roles_list['role'] = explode('|', $roles);

        foreach ($roles_list['role'] as $role)
        {
            if (Auth::user() && optional(Auth::user()->role)->slug == $role)
                return $next($request);
        }

        return redirect('home')->with('error', 'You don\'t have access');
    }
}
