<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MedicalRecordResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'id' => $this->id,
            'illness' => $this->illness,
            'data' => $this->data,
            'note' => $this->note,
            'patient_id' => $this->user_id,
            'medicaments' => MedicamentResource::collection($this->medicaments),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
