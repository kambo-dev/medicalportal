<?php


namespace App\Http\Services;


use App\Repositories\Contracts\RatingRepositoryContract;
use Exception;
use Illuminate\Support\Facades\Log;

class RatingService
{
    private static $ratingRepository;

    public function __construct(RatingRepositoryContract $ratingRepository)
    {
        self::$ratingRepository = $ratingRepository;
    }

    public static function store(Array $data) : bool
    {
        try {
            self::$ratingRepository->create($data);
            return true;
        } catch (Exception $e)
        {
            Log::error('RatingService::store Exception Error: ' . $e->getMessage());
            return false;
        }
    }
}
