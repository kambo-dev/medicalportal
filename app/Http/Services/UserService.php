<?php

namespace App\Http\Services;

use App\Repositories\Contracts\UserRepositoryContract;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserService
{
    private static $userRepository;

    public function __construct(UserRepositoryContract $userRepository)
    {
        self::$userRepository = $userRepository;
    }

    public static function store(array $data): bool
    {
        try {
            $data['password'] = Hash::make($data['password']);
            self::$userRepository->create($data);
            return true;
        }
        catch (Exception $e) {
            Log::error('ProductService::store Exception Error: ' . $e->getMessage());
            return false;
        }
    }

    public static function update(int $id, array $data): bool
    {
        try {
            self::$userRepository->findOrFail($id);
            $data['password'] = Hash::make($data['password']);
            self::$userRepository->update($data, $id);
            return true;
        } catch (Exception $e) {
            Log::error('ProductService::update Exception Error: ' . $e->getMessage());
            return false;
        }
    }
}
