<?php


namespace App\Http\Services;


use App\Repositories\Contracts\AppointmentRepositoryContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use Exception;
use Illuminate\Support\Facades\Log;

class AppointmentService
{
    private static $appointmentRepository;

    public function __construct(AppointmentRepositoryContract $appointmentRepository)
    {
        self::$appointmentRepository = $appointmentRepository;
    }

    public static function getAppointments(string $roles) : Collection
    {
        try {
            $role['list'] = explode('|', $roles);

            if (Auth::user()->hasRole($role['list']['0']))
            {
                return self::$appointmentRepository
                    ->getAppointmentsOfPatient(Auth::id(), 4);
            }
            if (Auth::user()->hasRole($role['list']['1']))
            {
                return self::$appointmentRepository
                    ->getAppointmentsOfDoctor(Auth::id(), 12);
            }
            else
                return Collection::empty();
        } catch (Exception $e) {
            Log::error('AppointmentService::getAppointments Exception Error: ' . $e->getMessage());
            return Collection::empty();
        }
    }

    public static function store(array $data) : bool
    {
        try {
            $data['date'] = substr($data['time'], 0, 10);
            self::$appointmentRepository->create($data);
            return true;
        } catch (Exception $e) {
            Log::error('AppointmentService::store Exception Error: ' . $e->getMessage());
            return false;
        }
    }
}
