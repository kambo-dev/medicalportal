<?php


namespace App\Http\Services;


use App\Repositories\Contracts\MedicalRecordRepositoryContract;
use App\Repositories\Contracts\MedicamentRepositoryContract;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MedicalRecordService
{
    private static $medicalRecordRepository;
    private static $medicamentRepository;

    public function __construct(MedicalRecordRepositoryContract $medicalRecordRepository,
                                MedicamentRepositoryContract $medicamentRepository)
    {
        self::$medicalRecordRepository = $medicalRecordRepository;
        self::$medicamentRepository = $medicamentRepository;
    }

    public static function store(array $data) : bool
    {
        try {
            DB::beginTransaction();
            $medicalRecord = self::$medicalRecordRepository->create(
                Arr::only($data, ['illness', 'data', 'note', 'user_id'])
            );

            if ($data['medicaments'])
            {
                foreach ($data['medicaments'] as $medicament)
                {
                    self::$medicamentRepository->create(
                        $medicament + ['medical_record_id' => $medicalRecord->id]
                    );
                }
            }

            DB::commit();
            return true;
        } catch (Exception $e)
        {
            DB::rollBack();
            Log::error('MedicalRecordService::store Exception Error: ' . $e->getMessage());
            return false;
        }
    }

    public static function update(array $data, int $id) : bool
    {
        try {
            DB::beginTransaction();
//            self::$medicalRecordRepository->findOrFail($id);

//            $medicalRecord = self::$medicalRecordRepository->update(
//                Arr::only($data, ['illness', 'data', 'note', 'user_id']),
//                $id
//            );
//
//            if ($data['medicaments'])
//            {
//                foreach ($data['medicaments'] as $medicament)
//                {
//                    self::$medicamentRepository->update(
//                        $medicament + ['medical_record_id' => $medicalRecord->id],
//                        $id
//                    );
//                }
//            }

            DB::commit();
            return true;
        } catch (Exception $e)
        {
            DB::rollBack();
            Log::error('MedicalRecordService::store Exception Error: ' . $e->getMessage());
            return false;
        }
    }
}
