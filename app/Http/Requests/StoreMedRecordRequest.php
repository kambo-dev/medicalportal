<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMedRecordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'illness' => ['required', 'string', 'max:20'],
            'data' => ['required', 'string', 'max:200'],
            'note' => ['required', 'string', 'max:50'],
            'medicaments.*.name' => ['required', 'string', 'max:20'],
            'medicaments.*.description' => ['required', 'string', 'max:50']
        ];
    }
}
