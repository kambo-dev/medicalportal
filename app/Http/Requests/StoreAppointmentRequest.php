<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoreAppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $now = date('Y-m-d H:i');
        $doctor_id = $this->request->get('doctor_id');
        return [
            'description' => ['max:200'],
            'time' => [
                'required',
                Rule::unique('appointments')->where(function ($query) use($doctor_id) {
                    return $query->where('doctor_id', $doctor_id);
                }),
                'after:'.$now
            ],
            'doctor_id' => ['required', 'string'],
//            'patient_id' => ['required', 'string'],
        ];
    }
}
