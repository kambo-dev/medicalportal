<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    protected function prepareForValidation()
    {
        $this->request->add(['appointment_id' => $this->appointment_id]);
    }

    public function rules()
    {
        return [
            'rating' => ['required', 'between:1,5'],
            'appointment_id' => ['required', 'unique:ratings,appointment_id'],
        ];
    }
}
