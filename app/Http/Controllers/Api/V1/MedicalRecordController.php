<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMedRecordRequest;
use App\Http\Resources\UserResource;
use App\Http\Services\MedicalRecordService;
use App\Repositories\Contracts\UserRepositoryContract;

class MedicalRecordController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index($patient_id)
    {
        $user = $this->userRepository->getMedicalRecords($patient_id);
        return new UserResource($user);
    }

    public function store(StoreMedRecordRequest $request, $patient_id)
    {
        $response = MedicalRecordService::store($request->validated() + ['user_id' => $patient_id]);
        if ($response)
        {
            return response()->json(['message' => 'Medical record created'], 200);
        }
        else return response()->json(['message' => 'Exception Error'], 400);
    }

    public function delete($id)
    {
        $medical_record = $this->userRepository->findOrFail($id);
        $medical_record->delete();
        return response()->json(['message' => 'Medical record removed'], 200);
    }
}
