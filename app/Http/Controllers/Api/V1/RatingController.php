<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRatingRequest;
use App\Http\Services\RatingService;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function store(StoreRatingRequest $request, $doctor_id, $appointment_id)
    {
        $response = RatingService::store($request->validated() + ['user_id' => $doctor_id]);
        if($response)
        {
            return response()->json(['message' => 'Appointment rated'], 200);
        } else return response()->json(['message' => 'Exception Error'], 400);
    }
}
