<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\RoleEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAppointmentRequest;
use App\Http\Resources\AppointmentResource;
use App\Http\Services\AppointmentService;
use App\Repositories\Contracts\AppointmentRepositoryContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    private $appointmentRepository;

    public function __construct(AppointmentRepositoryContract $appointmentRepository)
    {
        $this->appointmentRepository = $appointmentRepository;
    }

    public function index()
    {
        if (Auth::user()->hasRole(RoleEnum::PATIENT))
        {
            $appointments = $this->appointmentRepository
                ->getAppointmentsOfPatient(Auth::id(), 10);
        }
        elseif (Auth::user()->hasRole(RoleEnum::DOCTOR))
        {
            $appointments = $this->appointmentRepository
                ->getAppointmentsOfDoctor(Auth::id(), 20);
        }
        else
            $appointments = Collection::empty();

        return AppointmentResource::collection($appointments);
    }

    public function store(StoreAppointmentRequest $request)
    {
        $response = AppointmentService::store($request->validated() + ['patient_id' => strval(Auth::id())]);
        if ($response)
        {
            return response()->json(['message' => 'Appointment created'], 200);
        } else return response()->json(['message' => 'Exception Error'], 400);
    }

    public function delete(int $id)
    {
        $appointment = $this->appointmentRepository->findOrFail($id);
        $appointment->delete();
        return back();
    }
}
