<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\UserResource;
use App\Http\Services\UserService;
use App\Models\User;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return UserResource::collection(User::all());
    }

    public function store(StoreUserRequest $request)
    {
        $response = UserService::store($request->validated());
        if($response)
        {
            return response()->json(['message' => 'User created'], 200);
        } else return response()->json(['message' => 'Exception Error'], 400);
    }

    public function destroy(int $id)
    {
        $user = $this->userRepository->findUserByID($id);
        if ( !$user->trashed() )
            $user->delete();
        return response()->json(['message' => 'User deleted'], 200);
    }

    public function update(StoreUserRequest $request, $id)
    {
        $response = UserService::update($id, $request->validated());
        if ($response)
        {
            return response()->json(['message' => 'User edited'], 200);
        } else return response()->json(['message' => 'Exception Error'], 400);
    }

    public function restore($id)
    {
        $this->userRepository->findUserByID($id)->restore();
        return response()->json(['message' => 'User restored'], 200);
    }

    public function remove($id)
    {
        $this->userRepository->findUserByID($id)->forceDelete();
        return response()->json(['message' => 'User removed'], 200);
    }
}
