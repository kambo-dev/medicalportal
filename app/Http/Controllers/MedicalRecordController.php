<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMedRecordRequest;
use App\Http\Services\MedicalRecordService;
use App\Models\MedicalRecord;
use App\Repositories\Eloquent\UserRepository;

class MedicalRecordController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index($patient_id)
    {
        $user = $this->userRepository->getMedicalRecords($patient_id);
        return view('dashboard.medrecord', compact('user'));
    }

    public function store(StoreMedRecordRequest $request, $patient_id)
    {
        //Validated
        $response = MedicalRecordService::store($request->validated() + ['user_id' => $patient_id]);
        if ($response)
        {
            return back();
        } else return response()->json(['message' => 'Exception Error'], 400);
    }

//    public function update(StoreMedRecordRequest $request, $patient_id)
//    {
//        $response = MedicalRecordService::update($request->validated() + ['user_id' => $patient_id]);
//        if ($response)
//        {
//            return back();
//        } else return response()->json(['message' => 'Exception Error'], 400);
//    }

    public function delete(int $id)
    {
        $medicalRecord = MedicalRecord::findOrFail($id);
        $medicalRecord->delete();
        return back();
    }
}
