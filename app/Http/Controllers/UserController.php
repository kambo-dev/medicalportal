<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Http\Requests\StoreUserRequest;
use App\Models\Role;
use App\Repositories\Contracts\UserRepositoryContract;
use App\Http\Services\UserService;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request, UsersDataTable $dataTable)
    {
        $roles = Role::pluck('name', 'id');

        return $dataTable->render('users.index', compact('roles'));
    }

    public function store(StoreUserRequest $request)
    {
        $response = UserService::store($request->validated());
        if($response)
        {
            return back();
        } else return response()->json(['message' => 'Exception Error'], 400);
    }

    public function destroy(int $id)
    {
        $user = $this->userRepository->findUserByID($id);
        if ( !$user->trashed() )
            $user->delete();
        return back();
    }

    public function update(StoreUserRequest $request, $id)
    {
        $response = UserService::update($id, $request->validated());
        if ($response)
        {
            return back();
        } else return response()->json(['message' => 'Exception Error'], 400);
    }

    public function restore($id)
    {
        $this->userRepository->findUserByID($id)->restore();
        return back();
    }

    public function remove($id)
    {
        $this->userRepository->findUserByID($id)->forceDelete();
        return back();
    }

    public function getData($id)
    {
        // get records from database
        $arr['data'] = User::where('id', $id)->first();

        return response()->json($arr,200);
    }
}
