<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Http\Requests\StoreAppointmentRequest;
use App\Http\Services\AppointmentService;
use App\Models\Appointment;
use App\Repositories\Contracts\AppointmentRepositoryContract;
use App\Repositories\Contracts\UserRepositoryContract;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    private $userRepository;
    private $appointmentRepository;

    public function __construct(UserRepositoryContract $userRepository,
                                AppointmentRepositoryContract $appointmentRepository)
    {
        $this->userRepository = $userRepository;
        $this->appointmentRepository = $appointmentRepository;
    }

    public function index()
    {
        $fields = $this->userRepository->getDoctorFields();

        if (Auth::user()->hasRole(RoleEnum::PATIENT))
        {
            $appointments = $this->appointmentRepository
                ->getAppointmentsOfPatient(Auth::id(), 4);
        }
        elseif (Auth::user()->hasRole(RoleEnum::DOCTOR))
        {
            $appointments = $this->appointmentRepository
                ->getAppointmentsOfDoctor(Auth::id(), 12);
        }
        else
            $appointments = Collection::empty();

        return view('dashboard.book', compact('appointments', 'fields'));
    }

    public function getData($field, $time)
    {
        // get records from database
        $arr['data'] = $this->userRepository->getAvailableDoctors($field,
            Carbon::createFromFormat('Y-m-d H:i:s',
                str_replace('%20', ' ', $time.':00')
            )
        );
        return response()->json($arr, 200);
    }

    public function store(StoreAppointmentRequest $request)
    {
        $response = AppointmentService::store($request->validated() + ['patient_id' => strval(Auth::id())]);
        if ($response)
        {
            return back();
        } else return response()->json(['message' => 'Exception Error'], 400);
    }

    public function delete(int $id)
    {
        $appointment = Appointment::findOrFail($id);
        $appointment->delete();
        return back();
    }
}
