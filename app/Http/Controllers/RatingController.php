<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRatingRequest;
use App\Http\Services\RatingService;

class RatingController extends Controller
{
    public function store(StoreRatingRequest $request, $doctor_id, $appointment_id)
    {
        $response = RatingService::store(
            $request->validated() + ['user_id' => $doctor_id]
        );
        if($response)
        {
            return back();
        } else return response()->json(['message' => 'Exception Error'], 400);
    }
}
