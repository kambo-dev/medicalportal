<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Repositories\Contracts\AppointmentRepositoryContract;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $appointmentRepository;
    private $userRepository;

    public function __construct(AppointmentRepositoryContract $appointmentRepository,
                                UserRepositoryContract $userRepository)
    {
        $this->appointmentRepository = $appointmentRepository;
        $this->userRepository = $userRepository;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $active_appointments = $this->appointmentRepository->numOfActiveAppointments(Auth::id());
        $finished_appointments = $this->appointmentRepository->numOfFinishedAppointments(Auth::id());
        if (Auth::user()->hasRole(RoleEnum::DOCTOR))
        {
            $ratings = $this->userRepository->getRatingsOfUser(Auth::id());
            foreach ($ratings as $rating)
            {
                $avg = $rating->ratings()->avg('rating') * 20;
            }
        }
        else
        {
            $ratings = Collection::empty();
            $avg = 0;
        }
        return view('home', compact('active_appointments', 'finished_appointments', 'avg'));
    }

    public function calendar()
    {
        return view('dashboard.calendar');
    }
}
