<?php


namespace App\Repositories\Eloquent;


use App\Models\Appointment;
use App\Repositories\Contracts\AppointmentRepositoryContract;
use Illuminate\Support\Collection;

class AppointmentRepository extends BaseRepository implements AppointmentRepositoryContract
{
    public function model()
    {
        return Appointment::class;
    }

    /**
     * @param int $id
     * @param int $take
     * @return Collection
     */
    public function getAppointmentsOfPatient(int $id, int $take): Collection
    {
        return $this->model
            ->where('patient_id', $id)
            ->with('doctor')
            ->orderBy('updated_at', 'desc')
            ->take($take)
            ->get();
    }

    public function getAppointmentsOfDoctor(int $id, int $take): Collection
    {
        return $this->model
            ->where('doctor_id', $id)
            ->with('patient')
            ->orderBy('updated_at', 'desc')
            ->take($take)
            ->get();
    }

    public function numOfActiveAppointments(int $id) : int
    {
        return $this->model
            ->where('patient_id', $id)
            ->orWhere('doctor_id', $id)
            ->get()
            ->count();
    }

    public function numOfFinishedAppointments(int $id): int
    {
        return $this->model
            ->where('patient_id', $id)
            ->orWhere('doctor_id', $id)
            ->onlyTrashed()
            ->get()
            ->count();
    }
}
