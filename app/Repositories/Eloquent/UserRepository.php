<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository implements UserRepositoryContract
{

    public function model()
    {
        return User::class;
    }

    /**
     * @param string $field
     * @param \DateTime $time
     * @return mixed
     */
    public function getAvailableDoctors(string $field, \DateTime $time) : Collection
    {
        return $this->model
            ->doctor()
            ->with('doctorAppointments')
            ->where('field', $field)
            ->whereDoesntHave('doctorAppointments', function ($q) use ($time) {
                $q->where('time', $time);
            })
            ->take(10)
            ->get();
    }

    /**
     * @return Collection
     */
    public function getDoctorFields() : Collection
    {
        return $this->model
            ->select('field')
            ->doctor()
            ->where('field', '!=', null)
            ->where('field', '!=', '')
            ->get();
    }

    /**
     * @param int $id
     * @return User
     */
    public function findUserByID(int $id): User
    {
        return $this->model
            ->withTrashed()
            ->findOrFail($id);
    }

    public function getMedicalRecords(int $id): User
    {
        return $this->model
            ->where('id', $id)
            ->with(['medicalRecords' => function ($q) {
                $q->orderBy('created_at', 'desc');
            }, 'medicaments'])
            ->first();
    }

    public function getRatingsOfUser(int $id): Collection
    {
        return $this->model
            ->where('id', $id)
            ->with('ratings')
            ->get();
    }
}
