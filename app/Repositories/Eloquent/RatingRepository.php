<?php


namespace App\Repositories\Eloquent;


use App\Models\Rating;
use App\Repositories\Contracts\RatingRepositoryContract;

class RatingRepository extends BaseRepository implements RatingRepositoryContract
{
    public function model()
    {
        return Rating::class;
    }

}
