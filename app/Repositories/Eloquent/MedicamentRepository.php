<?php


namespace App\Repositories\Eloquent;


use App\Models\Medicament;
use App\Repositories\Contracts\MedicamentRepositoryContract;

class MedicamentRepository extends BaseRepository implements MedicamentRepositoryContract
{

    protected function model()
    {
        return Medicament::class;
    }
}
