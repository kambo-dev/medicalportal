<?php


namespace App\Repositories\Eloquent;


use App\Models\MedicalRecord;
use App\Repositories\Contracts\MedicalRecordRepositoryContract;

class MedicalRecordRepository extends BaseRepository implements MedicalRecordRepositoryContract
{
    public function model()
    {
        return MedicalRecord::class;
    }
}
