<?php

namespace App\Repositories\Contracts;

use App\Models\User;
use Illuminate\Support\Collection;
interface UserRepositoryContract extends BaseRepositoryContract
{
    /**
     * @param $field
     * @param $time
     * @return mixed
     */
    public function getAvailableDoctors(string $field, \DateTime $time) : Collection;

    /**
     * @return Collection
     */
    public function getDoctorFields() : Collection;

    /**
     * @param int $id
     * @return User
     */
    public function findUserByID(int $id) : User;

    /**
     * @param int $id
     * @return Collection
     */
    public function getMedicalRecords(int $id) : User;

    /**
     * @param int $id
     * @return User
     */
    public function getRatingsOfUser(int $id) : Collection;
}
