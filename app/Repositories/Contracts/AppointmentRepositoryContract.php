<?php


namespace App\Repositories\Contracts;


use Illuminate\Support\Collection;

interface AppointmentRepositoryContract extends  BaseRepositoryContract
{
    public function getAppointmentsOfPatient(int $id, int $take) : Collection;

    public function getAppointmentsOfDoctor(int $id, int $take) : Collection;

    public function numOfActiveAppointments(int $id) : int;

    public function numOfFinishedAppointments(int $id) : int;
}
